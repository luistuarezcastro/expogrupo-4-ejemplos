function animateElementX(element, start, end, duration) {
    return new Promise((resolve, reject) => {
        const delta = (end - start) * 30 / duration; //Distancia que va a recorrer cada elemento x 40 (milisegundos) / duración 
        element.style.left = start; //start parte desde la izquierda
        let counter = 0;
        const loop = setInterval(() => { //función itineraria
            const currentPosition = start + delta * counter++;
            element.style.left = currentPosition;
            if (start < end && currentPosition >= end) {
                clearInterval(loop);
                resolve();
            } else if (start > end && currentPosition <= end) {
                clearInterval(loop);
                resolve();
            }

        }, 30); // ejecuta una funcion cada cierto tiempo
    });
}

function animateElementY(element, start, end, duration) {
    return new Promise((resolve, reject) => {
        const delta = (end - start) * 30 / duration; //Distancia que va a recorrer cada elemento x 40 (milisegundos) / duración 
        element.style.top = start; //start parte desde la izquierda
        let counter = 0;
        const loop = setInterval(() => { //función itineraria
            const currentPosition = start + delta * counter++;
            element.style.top = currentPosition;
            if (start < end && currentPosition >= end) {
                clearInterval(loop);
                resolve();
            } else if (start > end && currentPosition <= end) {
                clearInterval(loop);
                resolve();
            }

        }, 30); // ejecuta una funcion cada cierto tiempo
    });
}


const allImg = document.getElementsByTagName("img");
/*MOVIMIENTO DEL ANDROIDE 18*/
const doggie = new Promise((resolve, reject) => {
    animateElementX(allImg[0], -200, 600, 3000) // se mueve hacia la derecha
        .then(() => {
            return animateElementY(allImg[0], 0, 400, 3000) // se mueve hacia abajo
        })
        .then(() => {
            return animateElementX(allImg[0], 600, 0, 3000) // se mueve hacia la izq
        })
        .then(() => {
            return animateElementY(allImg[0], 400, 0, 3000) // se mueve hacia arriba
        })
        .catch(() => {
            console.log("Falló la animación");
        })
});
/*MOVIMIENTO DEL ANDROIDE 17*/
const cate = new Promise((resolve, reject) => {
    animateElementX(allImg[1], -200, 400, 3100) // se mueve hacia la derecha
        .then(() => {
            return animateElementY(allImg[1], 140, 260, 3100) // se mueve hacia abajo
        })
        .then(() => {
            return animateElementX(allImg[1], 400, 0, 3100) // se mueve hacia la izq
        })
        .then(() => {
            return animateElementY(allImg[1], 260, 140, 3100) // se mueve hacia arriba
        })
        .catch(() => {
            console.log("Falló la animación");
        })
});


Promise.all([doggie, cate]);
